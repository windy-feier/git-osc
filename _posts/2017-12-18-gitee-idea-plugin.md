---
layout: post
title: "码云Idea插件更名为Gitee，并支持关联任务及创建代码片段"
---

### 1、插件更名为 Gitee，需要在市场搜索 Gitee 进行查找
![tupian](https://static.oschina.net/uploads/img/201712/01141617_wXCn.gif)

### 2、支持 Tasks 关联码云任务

通过插件，可将 IDEA Tasks 与码云 Issue 进行关联。

**Tasks 关联仓库的步骤如下**

1. 选择 `Tools` -> `Tasks && Contexts` -> `Configure Servers...` 
2. 点击 `+` 后选择 `GitOSC`，输入用户名和仓库名称，这两个名称需要与仓库 url 中的名称一致。
3. 点击`Create API Token`，输入邮箱和密码，点击 `Login`，此时会生成 Token。
4. 最后确定即可。

![输入图片说明](https://static.oschina.net/uploads/img/201711/24153109_HM6o.gif "在这里输入图片标题")

**打开 Tasks 步骤如下**

1. 选择 `Tools` -> `Tasks && Contexts` -> `Open Task...` 
1. 列表中会显示项目的 Issue，双击。
1. 设置仓库分支等，最后确定即可。

![输入图片说明](https://static.oschina.net/uploads/img/201711/24153121_DwTZ.gif "在这里输入图片标题")

**操作 Issue**
1. `Open Task...` Task 与 Issue 建立关联。
1. `Close Active Task` 可以更新 Issue 状态。
1. `Show '***' Description` 可显示 Issue 的描述信息。
1. `Open '***' In Browser` 可在浏览器中打开 Issue 页面。
![输入图片说明](https://static.oschina.net/uploads/img/201711/24153134_BaRU.png "在这里输入图片标题")

### 3、创建代码片段

打开源码文件，在文件窗口点击右键，选择`创建代码片段...`即可。

![输入图片说明](https://static.oschina.net/uploads/img/201711/24155948_eotH.png "在这里输入图片标题")

赶快来 [https://gitee.com/](https://gitee.com/) 体验吧！
