$(document).ready(function() {
  $('.content .log.title').on('click', function(){
    url =  $(location).attr('href').split('#')[0];
    position = $(this).find('a').attr('position');
    sessionStorage.gitee_log_parent_url = url + '#' + position;
  });

  $('.icon.go-back').parent().attr('href', sessionStorage.gitee_log_parent_url)
});
